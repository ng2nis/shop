import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

@Injectable()
export class ShopService {

  constructor(private _http: Http) { }
  baseUriAPI = 'https://jsonplaceholder.typicode.com/'

  // MARK: HTTP Methods

  get(uri: string): Observable<any> { 
    return this._http.get(this.baseUriAPI + uri)
    .map(this.extractData)
    .catch(this.handleError)
  }

  post() { }

  put() { }

  remove(uri: string): Observable<any> { 
    return this._http.delete(this.baseUriAPI + uri)
    .map(this.extractData)
    .catch(this.handleError)
  }






  // MARK: - Extract Data from Response

  private extractData = (res: Response) => {

    if (res.status < 200 || res.status>= 300) {
      throw new Error('Bad status ' + res.status)
    }

    let body = res.json()

    if (body.success == false) {
      throw new Error(body.message)
    }
    return body || {}
  }



  // MARK: Handle the errors 

  private handleError = (error: any) => {
    var message = error._body ? JSON.parse(error._body) : error.message
    return Observable.throw(message)
  }




}
