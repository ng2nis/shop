import { ShopService } from './../shop.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private _shop: ShopService) { }
  
  products: any
  isError = ''

  ngOnInit() {
    this._shop.get('photos?albumId=1').subscribe((data) => {
      this.products = data
    }, (error) => {
      this.isError = 'Server je trenutno prestao sa radom!'
    })
  }

}
