import { ShopService } from './../shop.service'
import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, Params } from '@angular/router'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(
    private _shop: ShopService, private _router: Router,
    private _route: ActivatedRoute
  ) { }
  
  id: number
  product: any

  ngOnInit() {
    this._route.params.subscribe( params => {
      if (params["id"]) {
        
        this._shop.get('photos/' + params['id']).subscribe((data) => {
          this.product = data
        }, (error) => {
        })
        
      }
    })
  }

}
