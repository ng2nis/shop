import { ShopService } from './shop.service'
import { Component, OnInit } from '@angular/core'

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  closeResult: string
  idForDelete = -1

  constructor(
    private _shop: ShopService, private modalService: NgbModal
  ) { }
  
  isError = ''
  products: any

  title = 'Foo bar'

  // MARK: - Initial method
  ngOnInit() {
    this._shop.get('photos?albumId=1').subscribe((data) => {
      this.products = data
    }, (error) => {
      this.isError = 'Server je trenutno prestao sa radom!'
    })
  }

  doDelete(i) {
    var id = this.products[i].id

    // if (!confirm("Da li ste sigurni?")) {
    //   return
    // }

    if (id > 0) {
      this._shop.remove('photos/' + id).subscribe((data) => {
        this.isError = 'Uspesno obrisan unos : ' + this.products[i].title
        this.products.splice(i, 1)

      }, (error) => {
        this.isError = 'Server je trenutno prestao sa radom!'
      })
    }

  }

  

  confirmDelete(content) {
    if (this.idForDelete >= 0) {
      this.doDelete(this.idForDelete)
      
      

    }
  }


  open(content, idForDelete) {
    this.idForDelete = idForDelete
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    })
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }



}
